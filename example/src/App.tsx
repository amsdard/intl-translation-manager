import React, { useState } from 'react';
import { FormattedMessage, FormattedHTMLMessage } from 'react-intl';
import { I18nProvider } from './context/i18n';
import { LanguageSelect } from './containers/LanguageSelect';
import { Form } from './containers/Form';
import './App.css';

const App: React.FC = () => {
  const [name, setName] = useState<string>('Tom');
  const [count, setCount] = useState<number>(1);
  return (
    <I18nProvider defaultLocale="en">
      <div className="App">
        <header className="header">
          <h1 className="header__title">
            <FormattedMessage
              id="header.title"
              description="Main header title"
              defaultMessage="I18n Example"
            />
          </h1>
          <section className="header__greeting">
            <FormattedMessage
              id="greeting.param"
              description="header.greeting"
              defaultMessage="Welcome {name}!"
              values={{ name }}
            />
          </section>
          <section className="header__messages">
            <FormattedHTMLMessage id='html.test' description="html" defaultMessage="hej ho <b>sad</b>" values={{t: 'let\'s go'}} />
            <FormattedMessage
              id="messages.plural"
              description="messages count info"
              defaultMessage="{count, plural, =0 {No messages:(} one {One new message} other {# new messages}}"
              values={{ count }}
            />
            <button onClick={() => setCount(Math.max(0, count - 1))}>-</button>
            <button onClick={() => setCount(count + 1)}>+</button>
          </section>
          <section className="header__language">
            <LanguageSelect />
          </section>
        </header>
        <Form onChange={(e: any) => setName(e.target.value)} value={name} />
      </div>
    </I18nProvider>
  );
};

export default App;
