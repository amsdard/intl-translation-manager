import React, {createContext, useContext, useState} from "react";
import { IntlProvider } from 'react-intl';
import { locales } from '../i18n';

type I18nProviderProps = {
    defaultLocale: string,
    children: JSX.Element,
}
type I18nContextType = {
    setLocale: (locale: string) => void,
    locale: string,
    locales: string[],
}

const I18nContext = createContext<I18nContextType>({locale: '', setLocale: () => {}, locales: []});
export const I18nProvider: React.FC<I18nProviderProps> = ({defaultLocale, children}) => {
    const [locale, setLocale] = useState<string>(defaultLocale);
    return (
        <I18nContext.Provider value={{ locale, setLocale, locales: Object.keys(locales) }}>
            <IntlProvider locale={locale} messages={locales[locale]}>
                { children }
            </IntlProvider>
        </I18nContext.Provider>
    )
};

export const useI18n = () => useContext(I18nContext);
