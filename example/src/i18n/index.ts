import { default as nl } from './locales/nl.json';
import { default as pl } from './locales/pl.json';
import { default as en } from './locales/en.json';

export const locales: Record<string, Record<string, string>> = {
    nl,
    pl,
    en,
};
