import React from 'react';
import { useIntl, defineMessages } from 'react-intl';
import {FormattedMessage} from "react-intl";

export const messages = defineMessages({
    ok: {
        id: 'common.ok',
        description: 'basic ok',
        defaultMessage: 'Ok'
    },
    cancel: {
        id: 'common.cancel',
        description: 'basic cancel',
        defaultMessage: 'Cancel'
    }
});

type FormProps = {
    onChange: any,
    value: any,
};

export const Form: React.FC<FormProps> = ({onChange, value}) => {
    const intl = useIntl();
    return (
        <section>
            <button><FormattedMessage {...messages.ok} /></button>
            <button><FormattedMessage {...messages.cancel} /></button>
            <input value={value} onChange={onChange} placeholder={intl.formatMessage({
                id: 'from.name.placeholder',
                description: 'text visible in name input when any text is filled',
                defaultMessage: 'Your name',
            })} />
        </section>
    )
};
