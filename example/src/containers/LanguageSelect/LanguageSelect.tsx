import React from "react";
import { FormattedMessage } from "react-intl";
import { useI18n } from "../../context/i18n";

export const LanguageSelect: React.FC<{}> = () => {
    const { locales, setLocale, locale } = useI18n();

    return (
        <div>
            <FormattedMessage
                id="change.language"
                description="language select label"
                defaultMessage="Change Language"
            />
            <select value={locale} onChange={(e) => {setLocale(e.target.value)}}>
                { locales.map(l => <option key={l} value={l}>{l}</option>) }
            </select>
        </div>
    )
};
