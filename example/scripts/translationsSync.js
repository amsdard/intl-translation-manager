const GoogleSpreadsheet = require('google-spreadsheet');
const { sync, googleSpreadSheetAdapterFactory } = require('../..');
const sheetCredentials = require('../translationsCredentials');

sync({
  locales: ['nl', 'en', 'pl','fr'],
  translationsDirectory: './src/i18n/locales',
  adapter: googleSpreadSheetAdapterFactory({
    GoogleSpreadsheet,
    ...sheetCredentials,
  }),
});
