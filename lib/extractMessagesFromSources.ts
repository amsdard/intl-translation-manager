import { readFile } from 'fs';
import { promisify } from 'util';
import { relative, join } from 'path';
import { chain, omit, prop } from 'ramda';
/// <reference types="recursive-readdir-async" />
import * as rra from 'recursive-readdir-async';
import { asyncMap } from './async';
import { getDescriptorsFromCode } from './extractMessageFromCode';
import { Message, MessageDescriptor } from './types';

type FileWithMessages = {
  fullname: string;
  name: string;
  messages: MessageDescriptor[];
};

export const reportMessages = (files: FileWithMessages[]): void => {
  files.forEach(file => {
    const relativeFilePath = relative(process.cwd(), file.fullname);
    console.info(`Messages extracted from ${relativeFilePath}:`);
    file.messages.forEach(message => console.info(`> ${message.id}`));
  });
};

export const reportDuplicates = (files: FileWithMessages[]): void => {
  const messagesFilesMap = files.reduce(
    (acc: Record<string, string[]>, file) => {
      const relativeFilePath = relative(process.cwd(), file.fullname);
      return file.messages.reduce(
        (acc2: Record<string, string[]>, message: MessageDescriptor) => ({
          ...acc2,
          [message.id]: acc2[message.id]
            ? [...acc2[message.id], relativeFilePath]
            : [relativeFilePath],
        }),
        acc,
      );
    },
    {},
  );

  Object.entries(messagesFilesMap)
    .filter(([, files]) => files.length > 1)
    .forEach(([messageId, files]) => {
      console.error(
        `Found message id (${messageId}) duplicates in following files`,
      );
      files.forEach(fileName => console.error(`> ${fileName}`));
    });
};

export const extractMessagesFromSources = async (srcDirectory: string): Promise<Message[]> => {
  const files = await rra.list(join(process.cwd(), srcDirectory), { extensions: true });
  const correctFiles = files
    .filter(x => /\.(js|ts|jsx|tsx)$/.test(x.fullname))
    .filter(x => !/\.(d|test|spec)\.(ts|js|tsx|jsx)$/.test(x.fullname));
  const filesWithMessages = await asyncMap(async file => {
    const code = ((await promisify(readFile)(file.fullname, {
      encoding: 'utf8',
    })) as unknown) as string;
    return {
      ...file,
      messages: getDescriptorsFromCode(code),
    };
  }, correctFiles);
  const filesWithFoundMessages = filesWithMessages.filter(
    file => file.messages.length,
  );
  reportMessages(filesWithFoundMessages);
  reportDuplicates(filesWithFoundMessages);
  return chain(prop('messages'), filesWithFoundMessages).map(
    (messageDescriptor: MessageDescriptor): Message => {
      // @ts-ignore
      return omit(['id'], {
        ...messageDescriptor,
        messageId: messageDescriptor.id,
        translation: messageDescriptor.defaultMessage,
        done: 'n',
      });
    },
  );
};
