import { promisify } from 'util';
import { Adapter } from './types';

type Callback = (err: any, data: any) => void;

interface IGoogleSpreadsheet {
  new (sheetId: string): IGoogleSpreadsheet;
  useServiceAccountAuth: (credentials: {}, cb: Callback) => void;
  getInfo: (cb: Callback) => void;
  addWorksheet: (data: {}, cb?: Callback) => void;
}
interface IGoogleWorksheet {
  getRows: (cb: Callback) => void;
  title: string;
  resize: any;
  setHeaderRow: any;
  addRow: any;
  clear: any;
}

interface IGoogleSpreadsheetInfo {
  worksheets: IGoogleWorksheet[];
}

interface IGoogleSpreadsheetRow {
  [key: string]: string;
}

type GoogleSpreadSheetConfig = {
  sheetId: string;
  credentials: {
    client_email: string,
      private_key: string,
  };
  GoogleSpreadsheet: IGoogleSpreadsheet;
};

export const googleSpreadSheetAdapterFactory = ({
  sheetId,
  credentials,
  GoogleSpreadsheet,
}: GoogleSpreadSheetConfig): Adapter => {
  const doc = new GoogleSpreadsheet(sheetId);
  async function login() {
    return new Promise((resolve, reject) => {
      doc.useServiceAccountAuth(credentials, (err, data) => {
        if (err) {
          return reject(err);
        }
        resolve(data);
      });
    });
  }
  async function getInfo(
    doc: IGoogleSpreadsheet,
  ): Promise<IGoogleSpreadsheetInfo> {
    return new Promise((resolve, reject) => {
      doc.getInfo((err, data) => {
        if (err) {
          return reject(err);
        }
        resolve(data);
      });
    });
  }
  async function getRows(
    worksheet: IGoogleWorksheet,
  ): Promise<IGoogleSpreadsheetRow[]> {
    return new Promise((resolve, reject) => {
      worksheet.getRows((err, data) => {
        if (err) {
          return reject(err);
        }
        resolve(data);
      });
    });
  }

  return {
    pull: async locale => {
      await login();
      const info = await getInfo(doc);
      const worksheet = info.worksheets.find(ws => ws.title === locale);
      if (!worksheet) {
        return [];
      }
      const rows = await getRows(worksheet);
      return rows.map(row => ({
        messageId: row.messageid,
        description: row.description,
        defaultMessage: row.defaultmessage,
        translation: row.translation,
        done: row.done,
      }));
    },
    push: async (locale, translations) => {
      await login();
      const info = await getInfo(doc);
      // @ts-ignore
      const worksheet =
        info.worksheets.find(ws => ws.title === locale) ||
        (await promisify<
          { title: string; rowCount: number; colCount: number },
          IGoogleWorksheet
        >(doc.addWorksheet.bind(doc))({
          title: locale,
          rowCount: 1,
          colCount: 5,
        }));
      await promisify(worksheet.resize)({ rowCount: 1, colCount: 5 });
      const rows = await getRows(worksheet);
      if (rows.length) {
        await promisify(worksheet.clear)();
      }
      await promisify(worksheet.setHeaderRow)([
        'messageId',
        'description',
        'defaultMessage',
        'translation',
        'done',
      ]);
      for (const translation of translations) {
        await promisify(worksheet.addRow)(translation);
      }
    },
  };
};
