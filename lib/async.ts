export const asyncReduce = async <I, O>(
    fn: (acc: O, v: I) => Promise<O>,
    initial: O,
    data: I[],
): Promise<O> => {
    let result = initial;
    for (let value of data) {
        result = await fn(result, value);
    }
    return result;
};

export const asyncMap = async <I, O>(
    fn: (x: I) => Promise<O>,
    data: I[],
): Promise<O[]> => {
    const reducer = async (acc: any, v: any) => {
        const mappedV = await fn(v);
        return [...acc, mappedV];
    };
    return asyncReduce(reducer, [], data);
};
