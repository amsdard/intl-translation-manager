import { writeFile } from 'fs';
import { promisify } from 'util';
import { mergeDeepLeft } from 'ramda';
import { SyncConfig, TestConfig, Message } from './types';
import { extractMessagesFromSources } from './extractMessagesFromSources';
export { googleSpreadSheetAdapterFactory } from './googleSpreadSheetAdapter';

const toIdMap = (list: Message[]) =>
  list.reduce(
    (acc, v) => ({
      ...acc,
      [v.messageId]: v,
    }),
    {},
  );

export async function sync({
  locales,
  srcDirectory = '/src',
  translationsDirectory,
  adapter: { pull, push },
}: SyncConfig) {
  async function addTranslations(
    locale: string,
    translations: Message[],
  ): Promise<void> {
    const translationsMap = translations.reduce(
      (acc, v) => ({
        ...acc,
        [v.messageId]: v.translation,
      }),
      {},
    );
    const data = JSON.stringify(translationsMap);
    return promisify(writeFile)(
      `${translationsDirectory}/${locale}.json`,
      data,
      {},
    );
  }

  async function syncLocale(locale: string): Promise<any> {
    const translationList = await pull(locale);
    const translationsMap = toIdMap(translationList);
    const synchronized = Object.values(mergeDeepLeft(
      translationsMap,
      messagesMap,
    ) as Record<string, Message>);
    await push(locale, synchronized);
    await addTranslations(locale, synchronized);
  }
  const messageList = await extractMessagesFromSources(srcDirectory);
  const messagesMap = toIdMap(messageList);
  for (const locale of locales) {
    await syncLocale(locale);
  }
}
export async function test({ srcDirectory = '/src' }: TestConfig) {
  console.log(`Extract messages ${require('../package.json').version}`)
  extractMessagesFromSources(srcDirectory)
    .then(messages => console.log(messages))
    .catch(error => console.error(error));
}
