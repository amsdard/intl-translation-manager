import { getDescriptorsFromCode } from './extractMessageFromCode';

const reactFormatters = `
import React, { useState } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { FormattedMessage as DefinedMessage, defineMessages as xx, FormatHTMLMessage } from 'react-intl';
import { commonMessages } from 'i18n';

export const messages = xx({
  'b': {
    id: 'mess.b',
    'description': 'desc.b',
    defaultMessage: 'def.b',
  }, 
  c: {
    'id': 'mess.c',
    'description': 'desc.c',
    defaultMessage: 'def.c',
    extra: 'asd'
  }
});
xx();

export type X = {
  x: string,
  v: number
}

export default function LoginView({ location }) {
  const [redirect, setRedirect] = useState(false);

  function handleLoginSuccess() {
    setRedirect(true);
  }

  const redirectTo = location.state ? location.state.from : '/';

  return redirect ? (
    <Redirect to={'redirectTo'} />
  ) : (
    <SimplePage
      links={
        <Link to="register">
          <DefinedMessage {...commonMessages.register} />
          <DefinedMessage id="mess.a" description="desc.a" defaultMessage="def.a" />
        </Link>
      }
      contentClassName="app-content--login">
      <div className="login-content">
        <div className="login-content-wrapper login-content-wrapper--border">
          <h2 className="login-content-title">
            <DefinedMessage {...commonMessages.login} />
            <FormatHTMLMessage id="html.a" description="html.a" defaultMessage="html.a" />
          </h2>
          <LoginForm onLoginSuccess={handleLoginSuccess} />
        </div>
      </div>
    </SimplePage>
  );
}
`;


const hookFormatters = `
import React from 'react';
import { connect } from 'react-redux';
import { useIntl, FormattedMessage } from 'react-intl';
import { equals } from 'ramda';

import { flashMessages$, MessageType } from 'app';
import { changeToolAction } from 'CAD';
import ExtCollapsiblePanel from 'components/ExtCollapsiblePanel/ExtCollapsiblePanel';
import ExtCheckbox from 'components/ExtCheckbox';
import {
  changePropertyAction as roofChangePropertyAction,
  fillRoofAction,
  setSameRoofSettingsAction,
  clearRoofAction,
  resetCalculationStateAction,
  firstRoofSelector,
  roofIndexByNameSelector,
  roofNameByIndexSelector,
  roofsSelector,
} from 'project/roofs';
import { activateRoofAction } from 'project/project';
import PanelsAutocomplete from 'project/containers/PanelsAutocomplete';
import { saveProjectThunk } from 'project/thunks/projectThunks';
import { roofSelectedPanelsCountSelector } from 'project/roofPanels';
import { cadToolSelector, ExtraTool } from 'project/cad';
import { calculatingEndAction, calculatingStartAction } from 'project/meta';
import { commonMessages } from 'i18n';

import {
  addLoads,
  addWarnings,
  hasCalculationErrors,
  textErrorMessage,
} from '../../../grid';
import { roofSettingsSelector } from '../../../roofs';
import { calculationsFetcher } from '../../EditorLayout/PriceInfo';
import './style.css';
//mappers
const mapStateToProps = (state, ownProps) => {
  const { roof } = ownProps;
  const { name, chosenPanel, windDeflectors } = roof;
  const roofIndex = roofIndexByNameSelector(state, name);
  const currentRoofSettings = roofSettingsSelector(state, name);
  const firstRoofSettings = roofSettingsSelector(
    state,
    firstRoofSelector(state).name,
  );

  return {
    state,
    roofIndex,
    windDeflectors,
    firstRoofSettings,
    panelsCount: roofSelectedPanelsCountSelector(state, { roofName: name }),
    sameSettings: equals(currentRoofSettings, firstRoofSettings),
    isFirstRoof: roofIndex === 0,
    isLastRoof: roofIndex === roofsSelector(state).length - 1,
    nextRoofName: roofNameByIndexSelector(state, roofIndex + 1),
    isPanelChosen: !!chosenPanel,
    roofName: name,
    bigRoof: roof.bigRoof,
    activeTool: cadToolSelector(state),
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  const { roof } = ownProps;
  const roofName = roof.name;

  return {
    activate: (name = roofName) =>
      dispatch(
        activateRoofAction({
          roofName: name,
          initializer: 'ui',
          target: 'panels',
        }),
      ),
    fill: () => dispatch(fillRoofAction({ roofName })),
    clear: () => dispatch(clearRoofAction({ roofName })),
    setSameRoofSettings: (settings, sameSettings) => {
      !sameSettings &&
        dispatch(setSameRoofSettingsAction({ roofName, settings }));
    },
    calculateBallast: state => {
      dispatch(calculatingStartAction());
      dispatch(saveProjectThunk()).then(() => {
        calculationsFetcher(state)
          .then(data => {
            addLoads(data);
            addWarnings(data);
            if (hasCalculationErrors(data)) {
              data.forEach(roofCalculation => {
                if (roofCalculation.system.error) {
                  throw new Error(roofCalculation.system.error);
                }
              });
            }
            dispatch(calculatingEndAction());
          })
          .catch(response => {
            dispatch(resetCalculationStateAction({ roofName }));
            flashMessages$.next({
              type: MessageType.ERROR,
              message: textErrorMessage(response.message),
            });
            dispatch(calculatingEndAction());
          });
      });
    },
    changeToolAction: tool => dispatch(changeToolAction({ tool, options: {} })),
  };
};

//Layout
const RoofPanel = props => {
  let {
    roofName,
    roofIndex,
    fill,
    clear,
    state,
    calculateBallast,
    panelContent,
    isPanelChosen,
    sameSettings,
    open,
    activate,
    nextRoofName,
    isFirstRoof,
    isLastRoof,
    setSameRoofSettings,
    firstRoofSettings,
    windDeflectors,
    changeWindDeflectors,
    panelsCount,
    activeTool,
  } = props;

  const { formatMessage } = useIntl();

  const CalculationButton = props => {
    return (
      <button
        className="ghost-button inverse"
        onClick={() => calculateBallast(state)}
        disabled={props.disabled}>
        <FormattedMessage
          id="cadEditorRoofPanel.calculateButton"
          description="Button, link to calculate ballast action"
          defaultMessage="Calculate ballast"
        />
      </button>
    );
  };

  const FillRoofButton = props => {
    return (
      <button
        className="ghost-button inverse"
        onClick={fill}
        disabled={props.disabled}>
        <FormattedMessage
          id="cadEditorRoofPanel.autoFill"
          description="Button, link to auto fill action"
          defaultMessage="Auto fill"
        />
      </button>
    );
  };

  const ClearRoofButton = props => (
    <button className="app-dock__btn-back" {...props}>
      <FormattedMessage
        id="cadEditorRoofPanel.clear"
        description="Button, link to clear action"
        defaultMessage="Clear"
      />
    </button>
  );

  const NextRoofButton = () => (
    <button
      className="app-dock__btn-back"
      onClick={() => activate(nextRoofName)}>
      <FormattedMessage
        id="cadEditorRoofPanel.nextRoof"
        description="Button, link to next roof tab"
        defaultMessage="Next roof"
      />
    </button>
  );

  const SettingsCheckbox = () => (
    <ExtCheckbox
      name="copy-options"
      label={formatMessage({
        id: 'cadEditorRoofPanel.sameSettings',
        description: 'Checkbox for setting the same settings as first roof',
        defaultMessage: 'Same settings as first roof',
      })}
      checked={sameSettings}
      className="full-width"
      onChange={() => setSameRoofSettings(firstRoofSettings, sameSettings)}
    />
  );

  const WindDeflectors = () => (
    <ExtCheckbox
      name="wind-deflectors"
      label={formatMessage({
        id: 'cadEditorRoofPanel.windDeflectors',
        description: 'Checkbox for setting the wind deflectors',
        defaultMessage: 'Wind deflectors',
      })}
      checked={!!windDeflectors}
      className="full-width"
      onChange={() => changeWindDeflectors(!windDeflectors)}
    />
  );

  const panelProps = {
    open,
    onOpen: activate,
    title: formatMessage(commonMessages.roof, { rid: roofIndex + 1 }),
    rightOpenHeaderContent: (
      <ClearRoofButton onClick={clear} disabled={!isPanelChosen} />
    ),
    rightCloseHeaderContent: <ClearRoofButton disabled={true} />,
    children: (
      <div className="app-dock__editor-dock--options">
        {!isFirstRoof && <SettingsCheckbox />}
        <PanelsAutocomplete roofName={roofName} />
        <WindDeflectors />
        {panelContent}
        <div className="panel__footer-buttons">
          <FillRoofButton
            disabled={!isPanelChosen || activeTool !== ExtraTool.INTERACTION}
          />
          <CalculationButton disabled={panelsCount <= 0} />
        </div>
        {!isLastRoof && <NextRoofButton />}
      </div>
    ),
  };

  return <ExtCollapsiblePanel {...panelProps} />;
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(RoofPanel);
`;

it('should works with react formatters with aliases or not', () => {
    const descriptors = getDescriptorsFromCode(reactFormatters);
    expect(descriptors).toHaveLength(3);
    descriptors.forEach(descriptor => {
        expect(descriptor).toMatchObject({
            id: expect.any(String),
            description: expect.any(String),
            defaultMessage: expect.any(String)
        });
    });
});

it('should works with react hook formatters', () => {
    const descriptors = getDescriptorsFromCode(hookFormatters);
    expect(descriptors).toHaveLength(6);

});


describe('hoc and render prop', () => {
    it('should works with intl as first argument and member expression call', () => {
        const code = `
function a1(intl) {
    intl.formatMessage({ id: 'a.id', description: 'a.desc', defaultMessage: 'a.mess'});
    intl.formatHTMLMessage({ id: 'b.id', description: 'b.desc', defaultMessage: 'b.mess'});
    intl.formatMessage(messages.a);
    intl.formatHTMLMessage(messages.b);
}
`;

        const descriptors = getDescriptorsFromCode(code);
        expect(descriptors).toHaveLength(2);
        descriptors.forEach(descriptor => {
            expect(descriptor).toMatchObject({
                id: expect.any(String),
                description: expect.any(String),
                defaultMessage: expect.any(String)
            });
        });
    });

    it('should works with intl as n argument and member expression call', () => {
        const code = `
function a1(a, b, intl, c) {
    intl.formatMessage({ id: 'a.id', description: 'a.desc', defaultMessage: 'a.mess'});
    intl.formatHTMLMessage({ id: 'b.id', description: 'b.desc', defaultMessage: 'b.mess'});
    intl.formatMessage(messages.a);
    intl.formatHTMLMessage(messages.b);
}
`;

        const descriptors = getDescriptorsFromCode(code);
        expect(descriptors).toHaveLength(2);
        descriptors.forEach(descriptor => {
            expect(descriptor).toMatchObject({
                id: expect.any(String),
                description: expect.any(String),
                defaultMessage: expect.any(String)
            });
        });
    });

    it('should works with intl as n argument with object destruction and member expression call', () => {
        const code = `
function a1(a, b, { intl, ...rest }, c) {
    intl.formatMessage({ id: 'a.id', description: 'a.desc', defaultMessage: 'a.mess'});
    intl.formatHTMLMessage({ id: 'b.id', description: 'b.desc', defaultMessage: 'b.mess'});
    intl.formatMessage(messages.a);
    intl.formatHTMLMessage(messages.b);
}
`;

        const descriptors = getDescriptorsFromCode(code);
        expect(descriptors).toHaveLength(2);
        descriptors.forEach(descriptor => {
            expect(descriptor).toMatchObject({
                id: expect.any(String),
                description: expect.any(String),
                defaultMessage: expect.any(String)
            });
        });
    });

    it('should works with intl as Nth argument with aliases in object destruction and member expression call', () => {
        const code = `
function a1(a, b, { intl: intl2 }, c) {
    intl2.formatMessage({ id: 'a.id', description: 'a.desc', defaultMessage: 'a.mess'});
    intl2.formatHTMLMessage({ id: 'b.id', description: 'b.desc', defaultMessage: 'b.mess'});
    intl2.formatMessage(messages.a);
    intl2.formatHTMLMessage(messages.b);
}
`;

        const descriptors = getDescriptorsFromCode(code);
        expect(descriptors).toHaveLength(2);
        descriptors.forEach(descriptor => {
            expect(descriptor).toMatchObject({
                id: expect.any(String),
                description: expect.any(String),
                defaultMessage: expect.any(String)
            });
        });
    });


    it('should works with formatters as nth argument with aliases object destruction and identifier call', () => {
        const code = `
function a1(a, b, { intl: { formatMessage, formatHTMLMessage } }, c) {
    formatMessage({ id: 'a.id', description: 'a.desc', defaultMessage: 'a.mess'});
    formatHTMLMessage({ id: 'b.id', description: 'b.desc', defaultMessage: 'b.mess'});
    formatMessage(messages.a);
    formatHTMLMessage(messages.b);
}
`;

        const descriptors = getDescriptorsFromCode(code);
        expect(descriptors).toHaveLength(2);
        descriptors.forEach(descriptor => {
            expect(descriptor).toMatchObject({
                id: expect.any(String),
                description: expect.any(String),
                defaultMessage: expect.any(String)
            });
        });
    });

    it('should works with formatters as nth argument with aliases object destruction and identifier call with aliases', () => {
        const code = `
function a1(a, b, { intl: { formatMessage: fm, formatHTMLMessage: fh } }, c) {
    fm({ id: 'a.id', description: 'a.desc', defaultMessage: 'a.mess'});
    fh({ id: 'b.id', description: 'b.desc', defaultMessage: 'b.mess'});
    fm(messages.a);
    fh(messages.b);
}
`;

        const descriptors = getDescriptorsFromCode(code);
        expect(descriptors).toHaveLength(2);
        descriptors.forEach(descriptor => {
            expect(descriptor).toMatchObject({
                id: expect.any(String),
                description: expect.any(String),
                defaultMessage: expect.any(String)
            });
        });
    });

    it('local object destruction', () => {
        const code = `
function a1(a, b, intl, c) {
    
    const { formatMessage, formatHTMLMessage } = intl;
    formatMessage({ id: 'a.id', description: 'a.desc', defaultMessage: 'a.mess'});
    formatHTMLMessage({ id: 'b.id', description: 'b.desc', defaultMessage: 'b.mess'});
    formatMessage(messages.a);
    formatHTMLMessage(messages.b);
}
`;

        const descriptors = getDescriptorsFromCode(code);
        expect(descriptors).toHaveLength(2);
        descriptors.forEach(descriptor => {
            expect(descriptor).toMatchObject({
                id: expect.any(String),
                description: expect.any(String),
                defaultMessage: expect.any(String)
            });
        });
    });

    it('local object destruction with alias alongside variable reassignment', () => {
        const code = `
function a1(a, b, {intl: intl2}, c) {
    
    const { formatMessage: fmAlias } = intl2;
    const formatHTMLMessageAlias = intl2.formatHTMLMessage;
    fmAlias({ id: 'a.id', description: 'a.desc', defaultMessage: 'a.mess'});
    formatHTMLMessageAlias({ id: 'b.id', description: 'b.desc', defaultMessage: 'b.mess'});
    formatMessage(messages.a);
    formatHTMLMessage(messages.b);
}
`;

        const descriptors = getDescriptorsFromCode(code);
        expect(descriptors).toHaveLength(2);
        descriptors.forEach(descriptor => {
            expect(descriptor).toMatchObject({
                id: expect.any(String),
                description: expect.any(String),
                defaultMessage: expect.any(String)
            });
        });
    });


    it('support for assigned arrow function', () => {
        const code = `
const a1 = (a, b, {intl: intl2}, c) => {
    
    const { formatMessage: fmAlias } = intl2;
    const formatHTMLMessageAlias = intl2.formatHTMLMessage;
    fmAlias({ id: 'a.id', description: 'a.desc', defaultMessage: 'a.mess'});
    formatHTMLMessageAlias({ id: 'b.id', description: 'b.desc', defaultMessage: 'b.mess'});
    formatMessage(messages.a);
    formatHTMLMessage(messages.b);
}
`;

        const descriptors = getDescriptorsFromCode(code);
        expect(descriptors).toHaveLength(2);
        descriptors.forEach(descriptor => {
            expect(descriptor).toMatchObject({
                id: expect.any(String),
                description: expect.any(String),
                defaultMessage: expect.any(String)
            });
        });
    });


    it('support for anonymous functions', () => {
        const code = `
test((a, b, {intl: intl2}, c) => {
    
    const { formatMessage: fmAlias } = intl2;
    const formatHTMLMessageAlias = intl2.formatHTMLMessage;
    fmAlias({ id: 'a.id', description: 'a.desc', defaultMessage: 'a.mess'});
    formatHTMLMessageAlias({ id: 'b.id', description: 'b.desc', defaultMessage: 'b.mess'});
    formatMessage(messages.a);
    formatHTMLMessage(messages.b);
})
`;

        const descriptors = getDescriptorsFromCode(code);
        expect(descriptors).toHaveLength(2);
        descriptors.forEach(descriptor => {
            expect(descriptor).toMatchObject({
                id: expect.any(String),
                description: expect.any(String),
                defaultMessage: expect.any(String)
            });
        });
    });

    it('support for anonymous function and anonymous formatters', () => {
        const code = `
test(({ formatMessage, formatHTMLMessage: alias }) => {
    formatMessage({ id: 'a.id', description: 'a.desc', defaultMessage: 'a.mess'});
    alias({ id: 'b.id', description: 'b.desc', defaultMessage: 'b.mess'});
    formatMessage(messages.a);
    formatHTMLMessage(messages.b);
})
`;

        const descriptors = getDescriptorsFromCode(code);
        expect(descriptors).toHaveLength(2);
        descriptors.forEach(descriptor => {
            expect(descriptor).toMatchObject({
                id: expect.any(String),
                description: expect.any(String),
                defaultMessage: expect.any(String)
            });
        });
    });

    it('render prop', () => {
        const code = `
        const Component = () => (
    <Intl>
        {
          ({formatMessage}) => <div>{formatMessage({ id: 'a.id', description: 'a.desc', defaultMessage: 'a.mess'})}</div> 
        }
    </Intl>
);
`;

        const descriptors = getDescriptorsFromCode(code);
        expect(descriptors).toHaveLength(1);
        descriptors.forEach(descriptor => {
            expect(descriptor).toMatchObject({
                id: expect.any(String),
                description: expect.any(String),
                defaultMessage: expect.any(String)
            });
        });
    });
});
