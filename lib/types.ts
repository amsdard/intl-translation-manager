export type Adapter = {
  push: (locale: string, messages: Message[]) => Promise<any>;
  pull: (locale: string) => Promise<Message[]>;
};

export type SyncConfig = {
  locales: string[];
  translationsDirectory: string;
  messagesDirectory: string;
  srcDirectory?: string;
  adapter: Adapter;
};
export type TestConfig = {
  srcDirectory?: string;
};
export type ReactIntlMessage = {
  id: string;
  description: string;
  defaultMessage: string;
  translation: string;
  done: string;
};
export type ReactIntlTranslations = Record<string, string>;
export type Message = {
  messageId: string;
  description: string;
  defaultMessage: string;
  translation: string;
  done: string;
};

export type MessageDescriptor = {
  id: string;
  description?: string;
  defaultMessage?: string;
};
