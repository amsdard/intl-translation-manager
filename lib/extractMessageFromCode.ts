import * as parser from '@babel/parser';
// @ts-ignore
import traverse from '@babel/traverse';

import { MessageDescriptor } from './types';

const getLocalImportSpecifierName = (
  ast: any,
  specifierName: string,
): string | undefined => {
  let name = undefined;
  traverse(ast, {
    ImportDeclaration: (path: any) => {
      if (path.node.source.value === 'react-intl') {
        const specifier = path.node.specifiers.find(
          (specifier: any) => specifier.imported.name === specifierName,
        );
        if (specifier) {
          name = specifier.local.name;
        }
      }
    },
  });
  return name;
};

const getDescriptorJSXAttributes = (node: any) => {
  const props = [
    'id',
    'description',
    'defaultMessage',
    'dynamicImport',
    'classProperties',
  ];
  return node.attributes.reduce(
    (acc: Partial<MessageDescriptor>, attribute: any) => {
      if (
        attribute.type === 'JSXAttribute' &&
        props.indexOf(attribute.name.name) > -1 &&
        attribute.value.type === 'StringLiteral'
      ) {
        return {
          ...acc,
          [attribute.name.name]: attribute.value.value,
        };
      }
      return acc;
    },
    {},
  );
};
const getDescriptorsFromJSXElement = (
  ast: any,
  elementName?: string,
): MessageDescriptor[] => {
  if (!elementName) {
    return [];
  }
  const descriptors: MessageDescriptor[] = [];
  traverse(ast, {
    JSXOpeningElement: (path: any) => {
      if (path.node.name.name === elementName) {
        descriptors.push(getDescriptorJSXAttributes(path.node));
      }
    },
  });
  return descriptors;
};

const descriptorFromObjectExpresion = (node: any): MessageDescriptor => {
  const props = ['id', 'description', 'defaultMessage'];
  return node.properties.reduce(
    (acc: Partial<MessageDescriptor>, prop: any) => {
      if (
        prop.value.type === 'StringLiteral' &&
        (prop.key.type === 'StringLiteral' || prop.key.type === 'Identifier')
      ) {
        const propName = prop.key.value || prop.key.name;
        if (props.indexOf(propName) > -1) {
          return {
            ...acc,
            [prop.key.value || prop.key.name]: prop.value.value,
          };
        }
        return acc;
      }
      return acc;
    },
    {},
  );
};

const getDescriptorsFormFunctionCallArgument = (
  node: any,
): MessageDescriptor[] => {
  if (!node.arguments.length || node.arguments[0].type !== 'ObjectExpression') {
    return [];
  }
  return node.arguments[0].properties.map((property: any) => {
    if (property.value.type !== 'ObjectExpression') {
      return {};
    }
    return descriptorFromObjectExpresion(property.value);
  });
};

const getDescriptorsFromFunctionCall = (
  ast: any,
  functionName?: string,
): MessageDescriptor[] => {
  if (!functionName) {
    return [];
  }
  const descriptorsArray: MessageDescriptor[][] = [];

  traverse(ast, {
    CallExpression: (path: any) => {
      if (path.node.callee.name === functionName) {
        // console.log(path.node);
        descriptorsArray.push(
          getDescriptorsFormFunctionCallArgument(path.node),
        );
      }
    },
  });

  return descriptorsArray.reduce(
    (acc: MessageDescriptor[], descriptors: MessageDescriptor[]) => {
      return [...acc, ...descriptors];
    },
    [],
  );
};

const getDescriptorsFromHookFunctionCall = (
  ast: any,
  hookName: string | undefined,
  formatterName: string,
): MessageDescriptor[] => {
  if (!hookName) {
    return [];
  }
  const descriptors: MessageDescriptor[] = [];

  let intlId: any;
  traverse(ast, {
    CallExpression: (path: any) => {
      if (path.node.callee.name === hookName) {
        if (path.parent.id) {
          intlId = path.parent.id;
        }
      }
    },
  });

  if (intlId && intlId.type === 'Identifier') {
    const intlName = intlId.name;
    traverse(ast, {
      CallExpression: (path: any) => {
        if (
          path.node.callee.type === 'MemberExpression' &&
          path.node.callee.object.name === intlName &&
          path.node.callee.property.name === formatterName &&
          path.node.arguments.length &&
          path.node.arguments[0].type === 'ObjectExpression'
        ) {
          descriptors.push(
            descriptorFromObjectExpresion(path.node.arguments[0]),
          );
        }
      },
    });
  } else if (intlId && intlId.type === 'ObjectPattern') {
    const localFormatterName = (
      intlId.properties.find(
        (property: any) => property.key.name === formatterName,
      ) || { value: {} }
    ).value.name;

    if (localFormatterName) {
      traverse(ast, {
        CallExpression: (path: any) => {
          if (
            path.node.callee.name === localFormatterName &&
            path.node.arguments.length &&
            path.node.arguments[0].type === 'ObjectExpression'
          ) {
            descriptors.push(
              descriptorFromObjectExpresion(path.node.arguments[0]),
            );
          }
        },
      });
    }
  }

  return descriptors;
};

const getLocalIntlNameOrNothing = (path: any): string | undefined => {
  const params = path.node.params || [];
  const strictIntlParamNode = params.find(
    (paramNode: any) =>
      paramNode.type === 'Identifier' && paramNode.name === 'intl',
  );
  const objectPropertyParamNode = (() => {
    const objectNode = params.find((paramNode: any) => {
      if (paramNode.type === 'ObjectPattern') {
        return paramNode.properties.find((objectProperty: any) => {
          return (
            objectProperty.type !== 'RestElement' &&
            objectProperty.key.name === 'intl' &&
            objectProperty.value.type === 'Identifier'
          );
        });
      }
    });
    if (objectNode) {
      return objectNode.properties.find(
        (objectProperty: any) =>
          objectProperty.type !== 'RestElement' &&
          objectProperty.key.name === 'intl',
      );
    }
  })();

  if (strictIntlParamNode) {
    return 'intl';
  } else if (objectPropertyParamNode) {
    return objectPropertyParamNode.value.name as string;
  }
};

const getLocalFormattersNames = (path: any): string[] => {
  const params = path.node.params || [];
  const formatters = ['formatMessage', 'formatHTMLMessage'];
  let localFormatterNames: string[] = [];
  const localIntlName = getLocalIntlNameOrNothing(path);

  if (localIntlName) {
    path.traverse({
      VariableDeclarator: (path: any) => {
        if (
          path.node.init &&
          path.node.init.name === localIntlName &&
          path.node.id.type === 'ObjectPattern'
        ) {
          path.node.id.properties
            .filter((node: any) => formatters.indexOf(node.key.name) >= 0)
            .map((node: any) => node.value.name)
            .forEach((name: string) => localFormatterNames.push(name));
        }
        if (
          path.node.init &&
          path.node.init.type === 'MemberExpression' &&
          path.node.init.object.name === localIntlName &&
          formatters.indexOf(path.node.init.property.name) >= 0
        ) {
          localFormatterNames.push(path.node.id.name);
        }
      },
    });
  } else {
    const intlParam = params.reduce((acc: any, param: any) => {
      if (!acc && param.type === 'ObjectPattern') {
        const hasFormatters = !!param.properties.find(
          (prop: any) => prop.type !== 'RestElement' && formatters.indexOf(prop.key.name) >= 0,
        );
        if (hasFormatters) {
          return param;
        }
        const intlParam = param.properties.find(
          (prop: any) =>
              prop.type !== 'RestElement' && prop.key.name === 'intl' && prop.value.type === 'ObjectPattern',
        );
        if (intlParam) {
          return intlParam.value;
        }
      }
      return acc;
    }, undefined);
    if (intlParam) {
      intlParam.properties
        .filter((node: any) => formatters.indexOf(node.key.name) >= 0)
        .map((node: any) => node.value.name)
        .forEach((name: string) => localFormatterNames.push(name));
    }
  }
  return localFormatterNames;
};

const getDescriptorsFromFunctionDefinitions = (
  ast: any,
): MessageDescriptor[] => {
  const descriptors: MessageDescriptor[] = [];
  const formatters = ['formatMessage', 'formatHTMLMessage'];
  const f = (path: any) => {
    const localIntlName = getLocalIntlNameOrNothing(path);
    const localFormatterNames = getLocalFormattersNames(path);
    if (localFormatterNames.length || localIntlName) {
      path.traverse({
        CallExpression: (path: any) => {
          if (
            (path.node.callee.type === 'Identifier' &&
              localFormatterNames.indexOf(path.node.callee.name) >= 0) ||
            (path.node.callee.type === 'MemberExpression' &&
              localFormatterNames.indexOf(path.node.callee.property.name) >=
                0) ||
            (path.node.callee.type === 'MemberExpression' &&
              path.node.callee.object.name === localIntlName &&
              formatters.indexOf(path.node.callee.property.name) >= 0)
          ) {
            if (path.node.arguments[0].type === 'ObjectExpression') {
              descriptors.push(
                descriptorFromObjectExpresion(path.node.arguments[0]),
              );
            }
          }
        },
      });
    }
  };
  traverse(ast, {
    FunctionDeclaration: f,
    ArrowFunctionExpression: f,
  });
  return descriptors;
};

export const getDescriptorsFromCode = (code: string): MessageDescriptor[] => {
  const ast = parser.parse(code, {
    sourceType: 'module',
    plugins: [
      'jsx',
      'typescript',
      'exportDefaultFrom',
      'dynamicImport',
      'classProperties',
    ],
  });

  const formattedMessageLocalName = getLocalImportSpecifierName(
    ast,
    'FormattedMessage',
  );
  const formattedHtmlMessageLocalName = getLocalImportSpecifierName(
    ast,
    'FormattedHTMLMessage',
  );
  const defineMessagesLocalName = getLocalImportSpecifierName(
    ast,
    'defineMessages',
  );
  const useIntlLocalName = getLocalImportSpecifierName(ast, 'useIntl');

  return [
    ...getDescriptorsFromJSXElement(ast, formattedMessageLocalName),
    ...getDescriptorsFromJSXElement(ast, formattedHtmlMessageLocalName),
    ...getDescriptorsFromFunctionCall(ast, defineMessagesLocalName),
    ...getDescriptorsFromHookFunctionCall(
      ast,
      useIntlLocalName,
      'formatMessage',
    ),
    ...getDescriptorsFromHookFunctionCall(
      ast,
      useIntlLocalName,
      'formatHTMLMessage',
    ),
    ...getDescriptorsFromFunctionDefinitions(ast),
  ].filter(x => x.id);
};
