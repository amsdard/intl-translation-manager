"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var fs_1 = require("fs");
var util_1 = require("util");
var path_1 = require("path");
var ramda_1 = require("ramda");
/// <reference types="recursive-readdir-async" />
var rra = require("recursive-readdir-async");
var async_1 = require("./async");
var extractMessageFromCode_1 = require("./extractMessageFromCode");
exports.reportMessages = function (files) {
    files.forEach(function (file) {
        var relativeFilePath = path_1.relative(process.cwd(), file.fullname);
        console.info("Messages extracted from " + relativeFilePath + ":");
        file.messages.forEach(function (message) { return console.info("> " + message.id); });
    });
};
exports.reportDuplicates = function (files) {
    var messagesFilesMap = files.reduce(function (acc, file) {
        var relativeFilePath = path_1.relative(process.cwd(), file.fullname);
        return file.messages.reduce(function (acc2, message) {
            var _a;
            return (__assign(__assign({}, acc2), (_a = {}, _a[message.id] = acc2[message.id]
                ? __spreadArrays(acc2[message.id], [relativeFilePath]) : [relativeFilePath], _a)));
        }, acc);
    }, {});
    Object.entries(messagesFilesMap)
        .filter(function (_a) {
        var files = _a[1];
        return files.length > 1;
    })
        .forEach(function (_a) {
        var messageId = _a[0], files = _a[1];
        console.error("Found message id (" + messageId + ") duplicates in following files");
        files.forEach(function (fileName) { return console.error("> " + fileName); });
    });
};
exports.extractMessagesFromSources = function (srcDirectory) { return __awaiter(void 0, void 0, void 0, function () {
    var files, correctFiles, filesWithMessages, filesWithFoundMessages;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, rra.list(path_1.join(process.cwd(), srcDirectory), { extensions: true })];
            case 1:
                files = _a.sent();
                correctFiles = files
                    .filter(function (x) { return /\.(js|ts|jsx|tsx)$/.test(x.fullname); })
                    .filter(function (x) { return !/\.(d|test|spec)\.(ts|js|tsx|jsx)$/.test(x.fullname); });
                return [4 /*yield*/, async_1.asyncMap(function (file) { return __awaiter(void 0, void 0, void 0, function () {
                        var code;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0: return [4 /*yield*/, util_1.promisify(fs_1.readFile)(file.fullname, {
                                        encoding: 'utf8',
                                    })];
                                case 1:
                                    code = (_a.sent());
                                    return [2 /*return*/, __assign(__assign({}, file), { messages: extractMessageFromCode_1.getDescriptorsFromCode(code) })];
                            }
                        });
                    }); }, correctFiles)];
            case 2:
                filesWithMessages = _a.sent();
                filesWithFoundMessages = filesWithMessages.filter(function (file) { return file.messages.length; });
                exports.reportMessages(filesWithFoundMessages);
                exports.reportDuplicates(filesWithFoundMessages);
                return [2 /*return*/, ramda_1.chain(ramda_1.prop('messages'), filesWithFoundMessages).map(function (messageDescriptor) {
                        // @ts-ignore
                        return ramda_1.omit(['id'], __assign(__assign({}, messageDescriptor), { messageId: messageDescriptor.id, translation: messageDescriptor.defaultMessage, done: 'n' }));
                    })];
        }
    });
}); };
