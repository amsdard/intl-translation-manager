"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var extractMessageFromCode_1 = require("./extractMessageFromCode");
var reactFormatters = "\nimport React, { useState } from 'react';\nimport { Link, Redirect } from 'react-router-dom';\nimport { FormattedMessage as DefinedMessage, defineMessages as xx, FormatHTMLMessage } from 'react-intl';\nimport { commonMessages } from 'i18n';\n\nexport const messages = xx({\n  'b': {\n    id: 'mess.b',\n    'description': 'desc.b',\n    defaultMessage: 'def.b',\n  }, \n  c: {\n    'id': 'mess.c',\n    'description': 'desc.c',\n    defaultMessage: 'def.c',\n    extra: 'asd'\n  }\n});\nxx();\n\nexport type X = {\n  x: string,\n  v: number\n}\n\nexport default function LoginView({ location }) {\n  const [redirect, setRedirect] = useState(false);\n\n  function handleLoginSuccess() {\n    setRedirect(true);\n  }\n\n  const redirectTo = location.state ? location.state.from : '/';\n\n  return redirect ? (\n    <Redirect to={'redirectTo'} />\n  ) : (\n    <SimplePage\n      links={\n        <Link to=\"register\">\n          <DefinedMessage {...commonMessages.register} />\n          <DefinedMessage id=\"mess.a\" description=\"desc.a\" defaultMessage=\"def.a\" />\n        </Link>\n      }\n      contentClassName=\"app-content--login\">\n      <div className=\"login-content\">\n        <div className=\"login-content-wrapper login-content-wrapper--border\">\n          <h2 className=\"login-content-title\">\n            <DefinedMessage {...commonMessages.login} />\n            <FormatHTMLMessage id=\"html.a\" description=\"html.a\" defaultMessage=\"html.a\" />\n          </h2>\n          <LoginForm onLoginSuccess={handleLoginSuccess} />\n        </div>\n      </div>\n    </SimplePage>\n  );\n}\n";
var hookFormatters = "\nimport React from 'react';\nimport { connect } from 'react-redux';\nimport { useIntl, FormattedMessage } from 'react-intl';\nimport { equals } from 'ramda';\n\nimport { flashMessages$, MessageType } from 'app';\nimport { changeToolAction } from 'CAD';\nimport ExtCollapsiblePanel from 'components/ExtCollapsiblePanel/ExtCollapsiblePanel';\nimport ExtCheckbox from 'components/ExtCheckbox';\nimport {\n  changePropertyAction as roofChangePropertyAction,\n  fillRoofAction,\n  setSameRoofSettingsAction,\n  clearRoofAction,\n  resetCalculationStateAction,\n  firstRoofSelector,\n  roofIndexByNameSelector,\n  roofNameByIndexSelector,\n  roofsSelector,\n} from 'project/roofs';\nimport { activateRoofAction } from 'project/project';\nimport PanelsAutocomplete from 'project/containers/PanelsAutocomplete';\nimport { saveProjectThunk } from 'project/thunks/projectThunks';\nimport { roofSelectedPanelsCountSelector } from 'project/roofPanels';\nimport { cadToolSelector, ExtraTool } from 'project/cad';\nimport { calculatingEndAction, calculatingStartAction } from 'project/meta';\nimport { commonMessages } from 'i18n';\n\nimport {\n  addLoads,\n  addWarnings,\n  hasCalculationErrors,\n  textErrorMessage,\n} from '../../../grid';\nimport { roofSettingsSelector } from '../../../roofs';\nimport { calculationsFetcher } from '../../EditorLayout/PriceInfo';\nimport './style.css';\n//mappers\nconst mapStateToProps = (state, ownProps) => {\n  const { roof } = ownProps;\n  const { name, chosenPanel, windDeflectors } = roof;\n  const roofIndex = roofIndexByNameSelector(state, name);\n  const currentRoofSettings = roofSettingsSelector(state, name);\n  const firstRoofSettings = roofSettingsSelector(\n    state,\n    firstRoofSelector(state).name,\n  );\n\n  return {\n    state,\n    roofIndex,\n    windDeflectors,\n    firstRoofSettings,\n    panelsCount: roofSelectedPanelsCountSelector(state, { roofName: name }),\n    sameSettings: equals(currentRoofSettings, firstRoofSettings),\n    isFirstRoof: roofIndex === 0,\n    isLastRoof: roofIndex === roofsSelector(state).length - 1,\n    nextRoofName: roofNameByIndexSelector(state, roofIndex + 1),\n    isPanelChosen: !!chosenPanel,\n    roofName: name,\n    bigRoof: roof.bigRoof,\n    activeTool: cadToolSelector(state),\n  };\n};\n\nconst mapDispatchToProps = (dispatch, ownProps) => {\n  const { roof } = ownProps;\n  const roofName = roof.name;\n\n  return {\n    activate: (name = roofName) =>\n      dispatch(\n        activateRoofAction({\n          roofName: name,\n          initializer: 'ui',\n          target: 'panels',\n        }),\n      ),\n    fill: () => dispatch(fillRoofAction({ roofName })),\n    clear: () => dispatch(clearRoofAction({ roofName })),\n    setSameRoofSettings: (settings, sameSettings) => {\n      !sameSettings &&\n        dispatch(setSameRoofSettingsAction({ roofName, settings }));\n    },\n    calculateBallast: state => {\n      dispatch(calculatingStartAction());\n      dispatch(saveProjectThunk()).then(() => {\n        calculationsFetcher(state)\n          .then(data => {\n            addLoads(data);\n            addWarnings(data);\n            if (hasCalculationErrors(data)) {\n              data.forEach(roofCalculation => {\n                if (roofCalculation.system.error) {\n                  throw new Error(roofCalculation.system.error);\n                }\n              });\n            }\n            dispatch(calculatingEndAction());\n          })\n          .catch(response => {\n            dispatch(resetCalculationStateAction({ roofName }));\n            flashMessages$.next({\n              type: MessageType.ERROR,\n              message: textErrorMessage(response.message),\n            });\n            dispatch(calculatingEndAction());\n          });\n      });\n    },\n    changeToolAction: tool => dispatch(changeToolAction({ tool, options: {} })),\n  };\n};\n\n//Layout\nconst RoofPanel = props => {\n  let {\n    roofName,\n    roofIndex,\n    fill,\n    clear,\n    state,\n    calculateBallast,\n    panelContent,\n    isPanelChosen,\n    sameSettings,\n    open,\n    activate,\n    nextRoofName,\n    isFirstRoof,\n    isLastRoof,\n    setSameRoofSettings,\n    firstRoofSettings,\n    windDeflectors,\n    changeWindDeflectors,\n    panelsCount,\n    activeTool,\n  } = props;\n\n  const { formatMessage } = useIntl();\n\n  const CalculationButton = props => {\n    return (\n      <button\n        className=\"ghost-button inverse\"\n        onClick={() => calculateBallast(state)}\n        disabled={props.disabled}>\n        <FormattedMessage\n          id=\"cadEditorRoofPanel.calculateButton\"\n          description=\"Button, link to calculate ballast action\"\n          defaultMessage=\"Calculate ballast\"\n        />\n      </button>\n    );\n  };\n\n  const FillRoofButton = props => {\n    return (\n      <button\n        className=\"ghost-button inverse\"\n        onClick={fill}\n        disabled={props.disabled}>\n        <FormattedMessage\n          id=\"cadEditorRoofPanel.autoFill\"\n          description=\"Button, link to auto fill action\"\n          defaultMessage=\"Auto fill\"\n        />\n      </button>\n    );\n  };\n\n  const ClearRoofButton = props => (\n    <button className=\"app-dock__btn-back\" {...props}>\n      <FormattedMessage\n        id=\"cadEditorRoofPanel.clear\"\n        description=\"Button, link to clear action\"\n        defaultMessage=\"Clear\"\n      />\n    </button>\n  );\n\n  const NextRoofButton = () => (\n    <button\n      className=\"app-dock__btn-back\"\n      onClick={() => activate(nextRoofName)}>\n      <FormattedMessage\n        id=\"cadEditorRoofPanel.nextRoof\"\n        description=\"Button, link to next roof tab\"\n        defaultMessage=\"Next roof\"\n      />\n    </button>\n  );\n\n  const SettingsCheckbox = () => (\n    <ExtCheckbox\n      name=\"copy-options\"\n      label={formatMessage({\n        id: 'cadEditorRoofPanel.sameSettings',\n        description: 'Checkbox for setting the same settings as first roof',\n        defaultMessage: 'Same settings as first roof',\n      })}\n      checked={sameSettings}\n      className=\"full-width\"\n      onChange={() => setSameRoofSettings(firstRoofSettings, sameSettings)}\n    />\n  );\n\n  const WindDeflectors = () => (\n    <ExtCheckbox\n      name=\"wind-deflectors\"\n      label={formatMessage({\n        id: 'cadEditorRoofPanel.windDeflectors',\n        description: 'Checkbox for setting the wind deflectors',\n        defaultMessage: 'Wind deflectors',\n      })}\n      checked={!!windDeflectors}\n      className=\"full-width\"\n      onChange={() => changeWindDeflectors(!windDeflectors)}\n    />\n  );\n\n  const panelProps = {\n    open,\n    onOpen: activate,\n    title: formatMessage(commonMessages.roof, { rid: roofIndex + 1 }),\n    rightOpenHeaderContent: (\n      <ClearRoofButton onClick={clear} disabled={!isPanelChosen} />\n    ),\n    rightCloseHeaderContent: <ClearRoofButton disabled={true} />,\n    children: (\n      <div className=\"app-dock__editor-dock--options\">\n        {!isFirstRoof && <SettingsCheckbox />}\n        <PanelsAutocomplete roofName={roofName} />\n        <WindDeflectors />\n        {panelContent}\n        <div className=\"panel__footer-buttons\">\n          <FillRoofButton\n            disabled={!isPanelChosen || activeTool !== ExtraTool.INTERACTION}\n          />\n          <CalculationButton disabled={panelsCount <= 0} />\n        </div>\n        {!isLastRoof && <NextRoofButton />}\n      </div>\n    ),\n  };\n\n  return <ExtCollapsiblePanel {...panelProps} />;\n};\n\nexport default connect(\n  mapStateToProps,\n  mapDispatchToProps,\n)(RoofPanel);\n";
it('should works with react formatters with aliases or not', function () {
    var descriptors = extractMessageFromCode_1.getDescriptorsFromCode(reactFormatters);
    expect(descriptors).toHaveLength(3);
    descriptors.forEach(function (descriptor) {
        expect(descriptor).toMatchObject({
            id: expect.any(String),
            description: expect.any(String),
            defaultMessage: expect.any(String)
        });
    });
});
it('should works with react hook formatters', function () {
    var descriptors = extractMessageFromCode_1.getDescriptorsFromCode(hookFormatters);
    expect(descriptors).toHaveLength(6);
});
describe('hoc and render prop', function () {
    it('should works with intl as first argument and member expression call', function () {
        var code = "\nfunction a1(intl) {\n    intl.formatMessage({ id: 'a.id', description: 'a.desc', defaultMessage: 'a.mess'});\n    intl.formatHTMLMessage({ id: 'b.id', description: 'b.desc', defaultMessage: 'b.mess'});\n    intl.formatMessage(messages.a);\n    intl.formatHTMLMessage(messages.b);\n}\n";
        var descriptors = extractMessageFromCode_1.getDescriptorsFromCode(code);
        expect(descriptors).toHaveLength(2);
        descriptors.forEach(function (descriptor) {
            expect(descriptor).toMatchObject({
                id: expect.any(String),
                description: expect.any(String),
                defaultMessage: expect.any(String)
            });
        });
    });
    it('should works with intl as n argument and member expression call', function () {
        var code = "\nfunction a1(a, b, intl, c) {\n    intl.formatMessage({ id: 'a.id', description: 'a.desc', defaultMessage: 'a.mess'});\n    intl.formatHTMLMessage({ id: 'b.id', description: 'b.desc', defaultMessage: 'b.mess'});\n    intl.formatMessage(messages.a);\n    intl.formatHTMLMessage(messages.b);\n}\n";
        var descriptors = extractMessageFromCode_1.getDescriptorsFromCode(code);
        expect(descriptors).toHaveLength(2);
        descriptors.forEach(function (descriptor) {
            expect(descriptor).toMatchObject({
                id: expect.any(String),
                description: expect.any(String),
                defaultMessage: expect.any(String)
            });
        });
    });
    it('should works with intl as n argument with object destruction and member expression call', function () {
        var code = "\nfunction a1(a, b, { intl, ...rest }, c) {\n    intl.formatMessage({ id: 'a.id', description: 'a.desc', defaultMessage: 'a.mess'});\n    intl.formatHTMLMessage({ id: 'b.id', description: 'b.desc', defaultMessage: 'b.mess'});\n    intl.formatMessage(messages.a);\n    intl.formatHTMLMessage(messages.b);\n}\n";
        var descriptors = extractMessageFromCode_1.getDescriptorsFromCode(code);
        expect(descriptors).toHaveLength(2);
        descriptors.forEach(function (descriptor) {
            expect(descriptor).toMatchObject({
                id: expect.any(String),
                description: expect.any(String),
                defaultMessage: expect.any(String)
            });
        });
    });
    it('should works with intl as Nth argument with aliases in object destruction and member expression call', function () {
        var code = "\nfunction a1(a, b, { intl: intl2 }, c) {\n    intl2.formatMessage({ id: 'a.id', description: 'a.desc', defaultMessage: 'a.mess'});\n    intl2.formatHTMLMessage({ id: 'b.id', description: 'b.desc', defaultMessage: 'b.mess'});\n    intl2.formatMessage(messages.a);\n    intl2.formatHTMLMessage(messages.b);\n}\n";
        var descriptors = extractMessageFromCode_1.getDescriptorsFromCode(code);
        expect(descriptors).toHaveLength(2);
        descriptors.forEach(function (descriptor) {
            expect(descriptor).toMatchObject({
                id: expect.any(String),
                description: expect.any(String),
                defaultMessage: expect.any(String)
            });
        });
    });
    it('should works with formatters as nth argument with aliases object destruction and identifier call', function () {
        var code = "\nfunction a1(a, b, { intl: { formatMessage, formatHTMLMessage } }, c) {\n    formatMessage({ id: 'a.id', description: 'a.desc', defaultMessage: 'a.mess'});\n    formatHTMLMessage({ id: 'b.id', description: 'b.desc', defaultMessage: 'b.mess'});\n    formatMessage(messages.a);\n    formatHTMLMessage(messages.b);\n}\n";
        var descriptors = extractMessageFromCode_1.getDescriptorsFromCode(code);
        expect(descriptors).toHaveLength(2);
        descriptors.forEach(function (descriptor) {
            expect(descriptor).toMatchObject({
                id: expect.any(String),
                description: expect.any(String),
                defaultMessage: expect.any(String)
            });
        });
    });
    it('should works with formatters as nth argument with aliases object destruction and identifier call with aliases', function () {
        var code = "\nfunction a1(a, b, { intl: { formatMessage: fm, formatHTMLMessage: fh } }, c) {\n    fm({ id: 'a.id', description: 'a.desc', defaultMessage: 'a.mess'});\n    fh({ id: 'b.id', description: 'b.desc', defaultMessage: 'b.mess'});\n    fm(messages.a);\n    fh(messages.b);\n}\n";
        var descriptors = extractMessageFromCode_1.getDescriptorsFromCode(code);
        expect(descriptors).toHaveLength(2);
        descriptors.forEach(function (descriptor) {
            expect(descriptor).toMatchObject({
                id: expect.any(String),
                description: expect.any(String),
                defaultMessage: expect.any(String)
            });
        });
    });
    it('local object destruction', function () {
        var code = "\nfunction a1(a, b, intl, c) {\n    \n    const { formatMessage, formatHTMLMessage } = intl;\n    formatMessage({ id: 'a.id', description: 'a.desc', defaultMessage: 'a.mess'});\n    formatHTMLMessage({ id: 'b.id', description: 'b.desc', defaultMessage: 'b.mess'});\n    formatMessage(messages.a);\n    formatHTMLMessage(messages.b);\n}\n";
        var descriptors = extractMessageFromCode_1.getDescriptorsFromCode(code);
        expect(descriptors).toHaveLength(2);
        descriptors.forEach(function (descriptor) {
            expect(descriptor).toMatchObject({
                id: expect.any(String),
                description: expect.any(String),
                defaultMessage: expect.any(String)
            });
        });
    });
    it('local object destruction with alias alongside variable reassignment', function () {
        var code = "\nfunction a1(a, b, {intl: intl2}, c) {\n    \n    const { formatMessage: fmAlias } = intl2;\n    const formatHTMLMessageAlias = intl2.formatHTMLMessage;\n    fmAlias({ id: 'a.id', description: 'a.desc', defaultMessage: 'a.mess'});\n    formatHTMLMessageAlias({ id: 'b.id', description: 'b.desc', defaultMessage: 'b.mess'});\n    formatMessage(messages.a);\n    formatHTMLMessage(messages.b);\n}\n";
        var descriptors = extractMessageFromCode_1.getDescriptorsFromCode(code);
        expect(descriptors).toHaveLength(2);
        descriptors.forEach(function (descriptor) {
            expect(descriptor).toMatchObject({
                id: expect.any(String),
                description: expect.any(String),
                defaultMessage: expect.any(String)
            });
        });
    });
    it('support for assigned arrow function', function () {
        var code = "\nconst a1 = (a, b, {intl: intl2}, c) => {\n    \n    const { formatMessage: fmAlias } = intl2;\n    const formatHTMLMessageAlias = intl2.formatHTMLMessage;\n    fmAlias({ id: 'a.id', description: 'a.desc', defaultMessage: 'a.mess'});\n    formatHTMLMessageAlias({ id: 'b.id', description: 'b.desc', defaultMessage: 'b.mess'});\n    formatMessage(messages.a);\n    formatHTMLMessage(messages.b);\n}\n";
        var descriptors = extractMessageFromCode_1.getDescriptorsFromCode(code);
        expect(descriptors).toHaveLength(2);
        descriptors.forEach(function (descriptor) {
            expect(descriptor).toMatchObject({
                id: expect.any(String),
                description: expect.any(String),
                defaultMessage: expect.any(String)
            });
        });
    });
    it('support for anonymous functions', function () {
        var code = "\ntest((a, b, {intl: intl2}, c) => {\n    \n    const { formatMessage: fmAlias } = intl2;\n    const formatHTMLMessageAlias = intl2.formatHTMLMessage;\n    fmAlias({ id: 'a.id', description: 'a.desc', defaultMessage: 'a.mess'});\n    formatHTMLMessageAlias({ id: 'b.id', description: 'b.desc', defaultMessage: 'b.mess'});\n    formatMessage(messages.a);\n    formatHTMLMessage(messages.b);\n})\n";
        var descriptors = extractMessageFromCode_1.getDescriptorsFromCode(code);
        expect(descriptors).toHaveLength(2);
        descriptors.forEach(function (descriptor) {
            expect(descriptor).toMatchObject({
                id: expect.any(String),
                description: expect.any(String),
                defaultMessage: expect.any(String)
            });
        });
    });
    it('support for anonymous function and anonymous formatters', function () {
        var code = "\ntest(({ formatMessage, formatHTMLMessage: alias }) => {\n    formatMessage({ id: 'a.id', description: 'a.desc', defaultMessage: 'a.mess'});\n    alias({ id: 'b.id', description: 'b.desc', defaultMessage: 'b.mess'});\n    formatMessage(messages.a);\n    formatHTMLMessage(messages.b);\n})\n";
        var descriptors = extractMessageFromCode_1.getDescriptorsFromCode(code);
        expect(descriptors).toHaveLength(2);
        descriptors.forEach(function (descriptor) {
            expect(descriptor).toMatchObject({
                id: expect.any(String),
                description: expect.any(String),
                defaultMessage: expect.any(String)
            });
        });
    });
    it('render prop', function () {
        var code = "\n        const Component = () => (\n    <Intl>\n        {\n          ({formatMessage}) => <div>{formatMessage({ id: 'a.id', description: 'a.desc', defaultMessage: 'a.mess'})}</div> \n        }\n    </Intl>\n);\n";
        var descriptors = extractMessageFromCode_1.getDescriptorsFromCode(code);
        expect(descriptors).toHaveLength(1);
        descriptors.forEach(function (descriptor) {
            expect(descriptor).toMatchObject({
                id: expect.any(String),
                description: expect.any(String),
                defaultMessage: expect.any(String)
            });
        });
    });
});
