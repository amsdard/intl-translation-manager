export declare const asyncReduce: <I, O>(fn: (acc: O, v: I) => Promise<O>, initial: O, data: I[]) => Promise<O>;
export declare const asyncMap: <I, O>(fn: (x: I) => Promise<O>, data: I[]) => Promise<O[]>;
