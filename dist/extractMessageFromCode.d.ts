import { MessageDescriptor } from './types';
export declare const getDescriptorsFromCode: (code: string) => MessageDescriptor[];
