import { Adapter } from './types';
declare type Callback = (err: any, data: any) => void;
interface IGoogleSpreadsheet {
    new (sheetId: string): IGoogleSpreadsheet;
    useServiceAccountAuth: (credentials: {}, cb: Callback) => void;
    getInfo: (cb: Callback) => void;
    addWorksheet: (data: {}, cb?: Callback) => void;
}
declare type GoogleSpreadSheetConfig = {
    sheetId: string;
    credentials: {
        client_email: string;
        private_key: string;
    };
    GoogleSpreadsheet: IGoogleSpreadsheet;
};
export declare const googleSpreadSheetAdapterFactory: ({ sheetId, credentials, GoogleSpreadsheet, }: GoogleSpreadSheetConfig) => Adapter;
export {};
