export declare type Adapter = {
    push: (locale: string, messages: Message[]) => Promise<any>;
    pull: (locale: string) => Promise<Message[]>;
};
export declare type SyncConfig = {
    locales: string[];
    translationsDirectory: string;
    messagesDirectory: string;
    srcDirectory?: string;
    adapter: Adapter;
};
export declare type TestConfig = {
    srcDirectory?: string;
};
export declare type ReactIntlMessage = {
    id: string;
    description: string;
    defaultMessage: string;
    translation: string;
    done: string;
};
export declare type ReactIntlTranslations = Record<string, string>;
export declare type Message = {
    messageId: string;
    description: string;
    defaultMessage: string;
    translation: string;
    done: string;
};
export declare type MessageDescriptor = {
    id: string;
    description?: string;
    defaultMessage?: string;
};
