"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var fs_1 = require("fs");
var util_1 = require("util");
var ramda_1 = require("ramda");
var extractMessagesFromSources_1 = require("./extractMessagesFromSources");
var googleSpreadSheetAdapter_1 = require("./googleSpreadSheetAdapter");
exports.googleSpreadSheetAdapterFactory = googleSpreadSheetAdapter_1.googleSpreadSheetAdapterFactory;
var toIdMap = function (list) {
    return list.reduce(function (acc, v) {
        var _a;
        return (__assign(__assign({}, acc), (_a = {}, _a[v.messageId] = v, _a)));
    }, {});
};
function sync(_a) {
    var locales = _a.locales, _b = _a.srcDirectory, srcDirectory = _b === void 0 ? '/src' : _b, translationsDirectory = _a.translationsDirectory, _c = _a.adapter, pull = _c.pull, push = _c.push;
    return __awaiter(this, void 0, void 0, function () {
        function addTranslations(locale, translations) {
            return __awaiter(this, void 0, void 0, function () {
                var translationsMap, data;
                return __generator(this, function (_a) {
                    translationsMap = translations.reduce(function (acc, v) {
                        var _a;
                        return (__assign(__assign({}, acc), (_a = {}, _a[v.messageId] = v.translation, _a)));
                    }, {});
                    data = JSON.stringify(translationsMap);
                    return [2 /*return*/, util_1.promisify(fs_1.writeFile)(translationsDirectory + "/" + locale + ".json", data, {})];
                });
            });
        }
        function syncLocale(locale) {
            return __awaiter(this, void 0, void 0, function () {
                var translationList, translationsMap, synchronized;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, pull(locale)];
                        case 1:
                            translationList = _a.sent();
                            translationsMap = toIdMap(translationList);
                            synchronized = Object.values(ramda_1.mergeDeepLeft(translationsMap, messagesMap));
                            return [4 /*yield*/, push(locale, synchronized)];
                        case 2:
                            _a.sent();
                            return [4 /*yield*/, addTranslations(locale, synchronized)];
                        case 3:
                            _a.sent();
                            return [2 /*return*/];
                    }
                });
            });
        }
        var messageList, messagesMap, _i, locales_1, locale;
        return __generator(this, function (_d) {
            switch (_d.label) {
                case 0: return [4 /*yield*/, extractMessagesFromSources_1.extractMessagesFromSources(srcDirectory)];
                case 1:
                    messageList = _d.sent();
                    messagesMap = toIdMap(messageList);
                    _i = 0, locales_1 = locales;
                    _d.label = 2;
                case 2:
                    if (!(_i < locales_1.length)) return [3 /*break*/, 5];
                    locale = locales_1[_i];
                    return [4 /*yield*/, syncLocale(locale)];
                case 3:
                    _d.sent();
                    _d.label = 4;
                case 4:
                    _i++;
                    return [3 /*break*/, 2];
                case 5: return [2 /*return*/];
            }
        });
    });
}
exports.sync = sync;
function test(_a) {
    var _b = _a.srcDirectory, srcDirectory = _b === void 0 ? '/src' : _b;
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_c) {
            console.log("Extract messages " + require('../package.json').version);
            extractMessagesFromSources_1.extractMessagesFromSources(srcDirectory)
                .then(function (messages) { return console.log(messages); })
                .catch(function (error) { return console.error(error); });
            return [2 /*return*/];
        });
    });
}
exports.test = test;
