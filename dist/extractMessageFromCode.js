"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var parser = require("@babel/parser");
// @ts-ignore
var traverse_1 = require("@babel/traverse");
var getLocalImportSpecifierName = function (ast, specifierName) {
    var name = undefined;
    traverse_1.default(ast, {
        ImportDeclaration: function (path) {
            if (path.node.source.value === 'react-intl') {
                var specifier = path.node.specifiers.find(function (specifier) { return specifier.imported.name === specifierName; });
                if (specifier) {
                    name = specifier.local.name;
                }
            }
        },
    });
    return name;
};
var getDescriptorJSXAttributes = function (node) {
    var props = [
        'id',
        'description',
        'defaultMessage',
        'dynamicImport',
        'classProperties',
    ];
    return node.attributes.reduce(function (acc, attribute) {
        var _a;
        if (attribute.type === 'JSXAttribute' &&
            props.indexOf(attribute.name.name) > -1 &&
            attribute.value.type === 'StringLiteral') {
            return __assign(__assign({}, acc), (_a = {}, _a[attribute.name.name] = attribute.value.value, _a));
        }
        return acc;
    }, {});
};
var getDescriptorsFromJSXElement = function (ast, elementName) {
    if (!elementName) {
        return [];
    }
    var descriptors = [];
    traverse_1.default(ast, {
        JSXOpeningElement: function (path) {
            if (path.node.name.name === elementName) {
                descriptors.push(getDescriptorJSXAttributes(path.node));
            }
        },
    });
    return descriptors;
};
var descriptorFromObjectExpresion = function (node) {
    var props = ['id', 'description', 'defaultMessage'];
    return node.properties.reduce(function (acc, prop) {
        var _a;
        if (prop.value.type === 'StringLiteral' &&
            (prop.key.type === 'StringLiteral' || prop.key.type === 'Identifier')) {
            var propName = prop.key.value || prop.key.name;
            if (props.indexOf(propName) > -1) {
                return __assign(__assign({}, acc), (_a = {}, _a[prop.key.value || prop.key.name] = prop.value.value, _a));
            }
            return acc;
        }
        return acc;
    }, {});
};
var getDescriptorsFormFunctionCallArgument = function (node) {
    if (!node.arguments.length || node.arguments[0].type !== 'ObjectExpression') {
        return [];
    }
    return node.arguments[0].properties.map(function (property) {
        if (property.value.type !== 'ObjectExpression') {
            return {};
        }
        return descriptorFromObjectExpresion(property.value);
    });
};
var getDescriptorsFromFunctionCall = function (ast, functionName) {
    if (!functionName) {
        return [];
    }
    var descriptorsArray = [];
    traverse_1.default(ast, {
        CallExpression: function (path) {
            if (path.node.callee.name === functionName) {
                // console.log(path.node);
                descriptorsArray.push(getDescriptorsFormFunctionCallArgument(path.node));
            }
        },
    });
    return descriptorsArray.reduce(function (acc, descriptors) {
        return __spreadArrays(acc, descriptors);
    }, []);
};
var getDescriptorsFromHookFunctionCall = function (ast, hookName, formatterName) {
    if (!hookName) {
        return [];
    }
    var descriptors = [];
    var intlId;
    traverse_1.default(ast, {
        CallExpression: function (path) {
            if (path.node.callee.name === hookName) {
                if (path.parent.id) {
                    intlId = path.parent.id;
                }
            }
        },
    });
    if (intlId && intlId.type === 'Identifier') {
        var intlName_1 = intlId.name;
        traverse_1.default(ast, {
            CallExpression: function (path) {
                if (path.node.callee.type === 'MemberExpression' &&
                    path.node.callee.object.name === intlName_1 &&
                    path.node.callee.property.name === formatterName &&
                    path.node.arguments.length &&
                    path.node.arguments[0].type === 'ObjectExpression') {
                    descriptors.push(descriptorFromObjectExpresion(path.node.arguments[0]));
                }
            },
        });
    }
    else if (intlId && intlId.type === 'ObjectPattern') {
        var localFormatterName_1 = (intlId.properties.find(function (property) { return property.key.name === formatterName; }) || { value: {} }).value.name;
        if (localFormatterName_1) {
            traverse_1.default(ast, {
                CallExpression: function (path) {
                    if (path.node.callee.name === localFormatterName_1 &&
                        path.node.arguments.length &&
                        path.node.arguments[0].type === 'ObjectExpression') {
                        descriptors.push(descriptorFromObjectExpresion(path.node.arguments[0]));
                    }
                },
            });
        }
    }
    return descriptors;
};
var getLocalIntlNameOrNothing = function (path) {
    var params = path.node.params || [];
    var strictIntlParamNode = params.find(function (paramNode) {
        return paramNode.type === 'Identifier' && paramNode.name === 'intl';
    });
    var objectPropertyParamNode = (function () {
        var objectNode = params.find(function (paramNode) {
            if (paramNode.type === 'ObjectPattern') {
                return paramNode.properties.find(function (objectProperty) {
                    return (objectProperty.type !== 'RestElement' &&
                        objectProperty.key.name === 'intl' &&
                        objectProperty.value.type === 'Identifier');
                });
            }
        });
        if (objectNode) {
            return objectNode.properties.find(function (objectProperty) {
                return objectProperty.type !== 'RestElement' &&
                    objectProperty.key.name === 'intl';
            });
        }
    })();
    if (strictIntlParamNode) {
        return 'intl';
    }
    else if (objectPropertyParamNode) {
        return objectPropertyParamNode.value.name;
    }
};
var getLocalFormattersNames = function (path) {
    var params = path.node.params || [];
    var formatters = ['formatMessage', 'formatHTMLMessage'];
    var localFormatterNames = [];
    var localIntlName = getLocalIntlNameOrNothing(path);
    if (localIntlName) {
        path.traverse({
            VariableDeclarator: function (path) {
                if (path.node.init &&
                    path.node.init.name === localIntlName &&
                    path.node.id.type === 'ObjectPattern') {
                    path.node.id.properties
                        .filter(function (node) { return formatters.indexOf(node.key.name) >= 0; })
                        .map(function (node) { return node.value.name; })
                        .forEach(function (name) { return localFormatterNames.push(name); });
                }
                if (path.node.init &&
                    path.node.init.type === 'MemberExpression' &&
                    path.node.init.object.name === localIntlName &&
                    formatters.indexOf(path.node.init.property.name) >= 0) {
                    localFormatterNames.push(path.node.id.name);
                }
            },
        });
    }
    else {
        var intlParam = params.reduce(function (acc, param) {
            if (!acc && param.type === 'ObjectPattern') {
                var hasFormatters = !!param.properties.find(function (prop) { return prop.type !== 'RestElement' && formatters.indexOf(prop.key.name) >= 0; });
                if (hasFormatters) {
                    return param;
                }
                var intlParam_1 = param.properties.find(function (prop) {
                    return prop.type !== 'RestElement' && prop.key.name === 'intl' && prop.value.type === 'ObjectPattern';
                });
                if (intlParam_1) {
                    return intlParam_1.value;
                }
            }
            return acc;
        }, undefined);
        if (intlParam) {
            intlParam.properties
                .filter(function (node) { return formatters.indexOf(node.key.name) >= 0; })
                .map(function (node) { return node.value.name; })
                .forEach(function (name) { return localFormatterNames.push(name); });
        }
    }
    return localFormatterNames;
};
var getDescriptorsFromFunctionDefinitions = function (ast) {
    var descriptors = [];
    var formatters = ['formatMessage', 'formatHTMLMessage'];
    var f = function (path) {
        var localIntlName = getLocalIntlNameOrNothing(path);
        var localFormatterNames = getLocalFormattersNames(path);
        if (localFormatterNames.length || localIntlName) {
            path.traverse({
                CallExpression: function (path) {
                    if ((path.node.callee.type === 'Identifier' &&
                        localFormatterNames.indexOf(path.node.callee.name) >= 0) ||
                        (path.node.callee.type === 'MemberExpression' &&
                            localFormatterNames.indexOf(path.node.callee.property.name) >=
                                0) ||
                        (path.node.callee.type === 'MemberExpression' &&
                            path.node.callee.object.name === localIntlName &&
                            formatters.indexOf(path.node.callee.property.name) >= 0)) {
                        if (path.node.arguments[0].type === 'ObjectExpression') {
                            descriptors.push(descriptorFromObjectExpresion(path.node.arguments[0]));
                        }
                    }
                },
            });
        }
    };
    traverse_1.default(ast, {
        FunctionDeclaration: f,
        ArrowFunctionExpression: f,
    });
    return descriptors;
};
exports.getDescriptorsFromCode = function (code) {
    var ast = parser.parse(code, {
        sourceType: 'module',
        plugins: [
            'jsx',
            'typescript',
            'exportDefaultFrom',
            'dynamicImport',
            'classProperties',
        ],
    });
    var formattedMessageLocalName = getLocalImportSpecifierName(ast, 'FormattedMessage');
    var formattedHtmlMessageLocalName = getLocalImportSpecifierName(ast, 'FormattedHTMLMessage');
    var defineMessagesLocalName = getLocalImportSpecifierName(ast, 'defineMessages');
    var useIntlLocalName = getLocalImportSpecifierName(ast, 'useIntl');
    return __spreadArrays(getDescriptorsFromJSXElement(ast, formattedMessageLocalName), getDescriptorsFromJSXElement(ast, formattedHtmlMessageLocalName), getDescriptorsFromFunctionCall(ast, defineMessagesLocalName), getDescriptorsFromHookFunctionCall(ast, useIntlLocalName, 'formatMessage'), getDescriptorsFromHookFunctionCall(ast, useIntlLocalName, 'formatHTMLMessage'), getDescriptorsFromFunctionDefinitions(ast)).filter(function (x) { return x.id; });
};
