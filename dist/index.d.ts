import { SyncConfig, TestConfig } from './types';
export { googleSpreadSheetAdapterFactory } from './googleSpreadSheetAdapter';
export declare function sync({ locales, srcDirectory, translationsDirectory, adapter: { pull, push }, }: SyncConfig): Promise<void>;
export declare function test({ srcDirectory }: TestConfig): Promise<void>;
