import { Message, MessageDescriptor } from './types';
declare type FileWithMessages = {
    fullname: string;
    name: string;
    messages: MessageDescriptor[];
};
export declare const reportMessages: (files: FileWithMessages[]) => void;
export declare const reportDuplicates: (files: FileWithMessages[]) => void;
export declare const extractMessagesFromSources: (srcDirectory: string) => Promise<Message[]>;
export {};
