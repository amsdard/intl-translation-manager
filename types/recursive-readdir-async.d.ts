declare module 'recursive-readdir-async' {
    type FileDescription = {
        fullname: string,
        name: string,
    };
    export function list(path: string, options?: {}): Promise<FileDescription[]>
}
